<?php
session_start();
include "koneksi.php";

// Notif Error
$Err = "";
if(isset ($_GET ["Err"]) && !empty ($_GET ["Err"])){
	switch ($_GET ["Err"]){
		case 1:
			$Err = "Username dan Password Kosong";
		break;
		case 2:
			$Err = "Username Kosong";
		break;
		case 3:
			$Err = "Password Kosong";
		break;
		case 4:
			$Err = "Password salah";
		break;
		case 5:
			$Err = "Username atau Password salah";
		break;
		case 6:
			$Err = "Maaf, Terjadi Kesalahan";
		break;
	}
}

// Notif
$Notif = "";
if(isset ($_GET["Notif"]) && !empty ($_GET["Notif"])){
	switch($_GET["Notif"]){
		case 1:
			$Notif = "User berhasil dibuat, silahkan Login";
		break;
	}
}
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="shortcut icon" href="aset/foto/logo.png">
        <title>PRESENCE | ERP LABORATORY</title>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="aset/materialize/css/materialize.min.css"  media="screen,projection"/>
        <script type="text/javascript" src="aset/materialize/js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="aset/materialize/js/materialize.min.js"></script>
				<link rel="stylesheet" href="css/style.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
    <body background="aset/foto/bg2.jpg">
			<div id="particles-js"></div>
			<!-- particles.js lib (JavaScript CodePen settings): https://github.com/VincentGarreau/particles.js -->
			<script src='js/particles.min.js'></script>
			<script src='js/stats.min.js'></script>



					<script  src="js/index.js"></script>

            <!--Navbar-->
            <br><br><br>
            <div class="container">
                <div class="row" style="margin-top: 0px">
                    <div class="col s12 m8 offset-m2 l6 offset-l3">
                        <div class="card-panel">
                            <center><img src="aset/foto/logo.png" width="300px"></center>
                            <div class="row valign-wrapper">
                                <div class="col s12">
                                    <div class="row">
                                        <form action="validate_login.php" method="post">
                                            <div class="input-field col s12">
                                                <i class="material-icons prefix">account_circle</i>
                                                <input name="Username" id="Username" type="text" class="validate" required="required">
                                                <label for="Username">Username</label>
                                            </div>
                                            <div class="input-field col s12">
                                                <i class="material-icons prefix">https</i>
                                                <input name="Password" id="Password" type="Password" class="validate" required="required">
                                                <label for="Password">Password</label>
                                            </div>
                                            <div class="input-field col s6">
                                                <input type="submit" value="Login" name="Login" class="btn blue darken-4" width="100px" height="100px">
                                            </div>
                                        </form>
																				<form action="index.php">
																					<div class="input-field col s6">
																							<input type="submit" value="ABSEN" name="submitAbsen" class="btn blue darken-4" width="300px" height="300px">
																					</div>
																				</form>
																				<tr>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </body>
</html>
