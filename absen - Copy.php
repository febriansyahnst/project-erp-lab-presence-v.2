<?php
session_start();
include "koneksi.php";
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="shortcut icon" href="aset/foto/favicon.png">
        <title>KEPROFESIAN | ERP</title>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="aset/materialize/css/materialize.min.css"  media="screen,projection"/>
        <script type="text/javascript" src="aset/materialize/js/materialize.min.js"></script>
				<link rel="stylesheet" href="css/style.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
    <body background="aset/foto/bg2.png" width="1024px">
			<div id="particles-js"></div>
			<!-- particles.js lib (JavaScript CodePen settings): https://github.com/VincentGarreau/particles.js -->
			<script src='js/particles.min.js'></script>
			<script src='js/stats.min.js'></script>



					<script  src="js/index.js"></script>

            <!--Navbar-->
            <br><br><br>
            <div class="container">
                <div class="row" style="margin-top: 100px">
                    <div class="col s12 m8 offset-m2 l6 offset-l3">
                        <div class="card-panel">
                            <center><img src="aset/foto/favicon.png" width="300px"></center>
                            <div class="row valign-wrapper">
                                <div class="col s12">
                                    <div class="row">
                                        <form name="formabsen" id="formabsen" action="validate_absen.php" method="post">
                                          <div class="input-field col s12">
                                              <i class="material-icons prefix">cast</i>
                                              <input name="uid" id="uid" onkeyup="submit(this,this.formabsen)" type="text" class="validate" autofocus="true" required="required">
                                              <label for="uid">TAP KTM ANDA</label>
                                          </div>
                                            <!-- <div class="input-field col s6">
                                                <input type="submit" value="Absen" name="Absen" class="btn blue darken-4" width="100px" height="100px">
                                            </div> -->
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="aset/materialize/js/jquery-3.2.1.min.js"></script>
            <script>

            $('uid').on('keyup',function(){
              if($(this).val().length >= 10){
                $('formabsen').submit();
              }
            });


            </script>
    </body>
</html>
