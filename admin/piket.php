<?php
session_start();
include "../koneksi.php";
include "auth_user.php";
 ?>
<!DOCTYPE html>
<html>
<head>
  <link rel="shortcut icon" href="../aset/foto/logo.png">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>PRESENCE | ERP LABORATORY</title>
    <!-- Favicon-->
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link href="assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="assets/plugins/node-waves/waves.css" rel="stylesheet" />
    <link href="assets/plugins/animate-css/animate.css" rel="stylesheet" />
    <link href="assets/plugins/morrisjs/morris.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/themes/all-themes.css" rel="stylesheet" />
</head>

<body class="theme-blue">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.php">DASHBOARD APLIKASI ERP LABORATORY</a>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="../aset/foto/logo.png" width="100px" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Administrator</div>
                    <div class="email"><?php echo $_SESSION['Username']; ?></div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="../logout.php"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <?php
            include "menu.php";
            ?>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2018 <a href="javascript:void(0);">ERP LABORATORY <br> DEVELOPED By YAN</a>.
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>JADWAL PIKET</h2>
            </div>
            <!-- Container -->
            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                          <div class="row clearfix">
                            <div class="col-xs-12 col-sm-4">
                              <h4>Input Jadwal Piket</h4>
                            </div>
                            <!-- <div class="col-xs-12 col-sm-3 right">
                              <a href="presensi_empty.php"><button type="button" class="btn bg-red btn-block waves-effect">HAPUS SELURUH ABSENSI</button></a>
                            </div> -->
                          </div>
                          <form action="piket_add.php" method="post">
                            <div class="row clearfix">
                              <div class="col-xs-12 col-sm-3">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="kode_asisten" class="form-control" placeholder="KODE ASISTEN"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="jadwal" class="form-control" placeholder="JADWAL PIKET"/>
                                    </div>
                                </div>
                              </div>
                                <div class="col-xs-12 col-sm-3">
                                  <button class="btn bg-cyan btn-block waves-effect" type="submit" name="submit">SUBMIT</button>
                                </div>
                            </div>
                          </form>
                        </div>
                        <div class="body table-responsive">
                          <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                            <th>ID</th>
                            <th>KODE ASISTEN</th>
                            <th>NAMA</th>
                            <th>DVISI</th>
                            <th>JADWAL PIKET</th>
                            <th>ACTION</th>
                          </tr>
                        </thead>

                          <tbody>
                            <?php
                            $order = 1;
                            $querypiket = mysqli_query($konek, "SELECT * FROM piket INNER JOIN asisten ON piket.kode_asisten = asisten.kode_asisten ");
                            while($data = mysqli_fetch_array($querypiket)){
                              ?>
                              <tr>
                              <td><?php echo $order++ ?></td>
                              <td><?php echo $data['kode_asisten'] ?></td>
                              <td><?php echo $data['nama'] ?></td>
                              <td><?php echo $data['divisi'] ?></td>
                              <td><?php echo $data['jadwal'] ?></td>
                              <td><a href="piket_delete.php?kode_asisten=<?php echo $data['kode_asisten'] ?>        "><button type="button" class="btn bg-red btn-block waves-effect">DELETE</button></a></td>
                            </tr>
                            <?php
                            }
                             ?>
                          </tbody>
                          </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Container -->
            </div>
        </div>
    </section>
    <script src="assets/plugins/jquery/jquery.min.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.js"></script>
    <script src="assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <script src="assets/plugins/node-waves/waves.js"></script>
    <script src="assets/plugins/jquery-countto/jquery.countTo.js"></script>
    <script src="assets/plugins/raphael/raphael.min.js"></script>
    <script src="assets/plugins/morrisjs/morris.js"></script>
    <script src="assets/plugins/chartjs/Chart.bundle.js"></script>
    <script src="assets/plugins/flot-charts/jquery.flot.js"></script>
    <script src="assets/plugins/flot-charts/jquery.flot.resize.js"></script>
    <script src="assets/plugins/flot-charts/jquery.flot.pie.js"></script>
    <script src="assets/plugins/flot-charts/jquery.flot.categories.js"></script>
    <script src="assets/plugins/flot-charts/jquery.flot.time.js"></script>
    <script src="assets/plugins/jquery-sparkline/jquery.sparkline.js"></script>
    <script src="assets/js/admin.js"></script>
    <script src="assets/js/pages/index.js"></script>
    <script src="assets/js/demo.js"></script>
</body>

</html>
