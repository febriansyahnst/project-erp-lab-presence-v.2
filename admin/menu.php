<html>
<div class="menu">
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    <li class="active">
                        <a href="index.php">
                            <i class="material-icons">home</i>
                            <span>Home</span>
                        </a>
                    </li>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">assignment</i>
                            <span>Master Data</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="asisten.php">Asisten</a>
                                <a href="piket.php">Jadwal Piket</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">view_list</i>
                            <span>Presensi</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="Presensi.php">Presensi</a>
                            </li>
                            <li>
                                <a href="perfect_attendance.php">Perfect Attendance</a>
                            </li>
                            <li>
                                <a href="rekapasisten.php">Rekap Per Asisten</a>
                            </li>
                            <li>
                                <a href="rekapperiode.php">Rekap Periode</a>
                            </li>
                            <!-- <li>
                                <a href="cek_piket.php">Cek Piket</a>
                            </li> -->
                        </ul>
                    </li>
                    <li>
                        <a href="hbd.php">
                            <i class="material-icons">date_range</i>
                            <span>Cek Ulang Tahun</span>
                        </a>
                    </li>
                    <li>
                        <a href="user.php">
                            <i class="material-icons">account_circle</i>
                            <span>Users</span>
                        </a>
                    </li>
                </ul>
            </div>
            </html>