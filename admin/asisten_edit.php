<?php
session_start();
include "../koneksi.php";
include "auth_user.php";

$kode_asisten = $_GET['kode_asisten'];
$query = mysqli_query($konek, "SELECT * FROM asisten WHERE kode_asisten = '$kode_asisten'");
$data = mysqli_fetch_array($query);
 ?>
﻿<!DOCTYPE html>
<html>
<head>
  <link rel="shortcut icon" href="../aset/foto/logo.png">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>PRESENCE | ERP LABORATORY</title>
    <!-- Favicon-->
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link href="assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="assets/plugins/node-waves/waves.css" rel="stylesheet" />
    <link href="assets/plugins/animate-css/animate.css" rel="stylesheet" />
    <link href="assets/plugins/morrisjs/morris.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/themes/all-themes.css" rel="stylesheet" />
</head>

<body class="theme-blue">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.php">DASHBOARD APLIKASI ERP LABORATORY</a>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="../aset/foto/logo.png" width="100px" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Administrator</div>
                    <div class="email"><?php echo $_SESSION['Username']; ?></div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="../logout.php"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <?php
            include "menu.php";
            ?>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2018 <a href="javascript:void(0);">ERP LABORATORY <br> DEVELOPED By YAN</a>.
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>MASTER DATA : ASISTEN</h2>
            </div>
            <!-- Container -->
            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-3">
                                  <a href="asisten.php"><button type="button" class="btn bg-cyan btn-block waves-effect">VIEW ASISTEN</button></a>
                                </div>
                            </div>
                        </div>
                        <form action="asisten_edit.php" method="post">
                        <div class="body">
                            <div class="row clearfix">
                              <div class="col-md-2">
                                  <div class="input-group">
                                      <span class="input-group-addon">
                                          <i class="material-icons">credit_card</i>
                                      </span>
                                      <div class="form-line">
                                          <input type="text" name="uid" value="<?php echo $data['uid'] ?>" class="form-control date" placeholder="UID/RFID">
                                      </div>
                                  </div>
                              </div>
                                <div class="col-md-2">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">credit_card</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" name="kode_asisten" readonly value="<?php echo $data['kode_asisten'] ?>" placeholder="KODE ASISTEN">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">account_circle</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="text" name="nama" value="<?php echo $data['nama'] ?>"class="form-control date" placeholder="NAMA" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">event</i>
                                        </span>
                                        <div class="form-line">
                                            <input type="date" name="tgl_lahir" value="<?php echo $data['tgl_lahir'] ?>"class="form-control date" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-4">
                                  <h5>Jenis Kelamin</h5>
                                    <div class="demo-radio-button">
                                      <input name="jk" type="radio" id="L" value="L" class="with-gap"/>
                                        <label for="L">Pria</label>
                                      <input name="jk" type="radio" id="P" value="P" class="with-gap"/>
                                        <label for="P">Wanita</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="input-group">
                                      <span class="input-group-addon">
                                          <i class="material-icons">account_balance</i>
                                      </span>
                                      <div class="form-line">
                                          <input type="text" name="divisi" value="<?php echo $data['divisi'] ?>"class="form-control date" placeholder="DIVISI" required>
                                      </div>
                                  </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                              <div class="col-md-4">
                              <button class="btn bg-cyan btn-block waves-effect" type="submit" name="update">UPDATE</button>
                            </div>
                            </div>
                        </div>
                      </form>
                      <?php
                      if(isset($_POST['update'])){
                          $uid = $_POST['uid'];
                          $kode_asisten = $_POST['kode_asisten'];
                          $nama = $_POST['nama'];
                          $tgl_lahir = $_POST['tgl_lahir'];
                          $jk = $_POST['jk'];
                          $divisi = $_POST['divisi'];


                          $update = mysqli_query($konek, "UPDATE asisten SET uid='$uid',nama='$nama',tgl_lahir='$tgl_lahir',jk='$jk',divisi='$divisi' WHERE kode_asisten = '$kode_asisten'");
                          if ($update){
                            $update_uid = mysqli_query($konek, "UPDATE absen set uid='$uid' WHERE kode_asisten='$kode_asisten'");
                            if($update_uid)
                            {
                              echo "<script>alert('Data Berhasil Diupdate');location.href='asisten.php';</script>";
                            }
                          }else {
                            echo "<script>alert('Data Gagal Diupdate');location.href='asisten_edit.php?kode_asisten=$kode_asisten';</script>";
                          }
                        }

                       ?>
                    </div>
                </div>
            <!-- #END# Container -->
            </div>
        </div>
    </section>
    <script src="assets/plugins/jquery/jquery.min.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.js"></script>
    <script src="assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <script src="assets/plugins/node-waves/waves.js"></script>
    <script src="assets/plugins/jquery-countto/jquery.countTo.js"></script>
    <script src="assets/plugins/raphael/raphael.min.js"></script>
    <script src="assets/plugins/morrisjs/morris.js"></script>
    <script src="assets/plugins/chartjs/Chart.bundle.js"></script>
    <script src="assets/plugins/flot-charts/jquery.flot.js"></script>
    <script src="assets/plugins/flot-charts/jquery.flot.resize.js"></script>
    <script src="assets/plugins/flot-charts/jquery.flot.pie.js"></script>
    <script src="assets/plugins/flot-charts/jquery.flot.categories.js"></script>
    <script src="assets/plugins/flot-charts/jquery.flot.time.js"></script>
    <script src="assets/plugins/jquery-sparkline/jquery.sparkline.js"></script>
    <script src="assets/js/admin.js"></script>
    <script src="assets/js/pages/index.js"></script>
    <script src="assets/js/demo.js"></script>
</body>

</html>
