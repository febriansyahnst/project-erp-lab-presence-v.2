<?php
session_start();
include "../koneksi.php";
include "auth_user.php";
 ?>
﻿<!DOCTYPE html>
<html>
<head>
  <link rel="shortcut icon" href="../aset/foto/logo.png">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>PRESENCE | ERP LABORATORY</title>
    <!-- Favicon-->
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link href="assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="assets/plugins/node-waves/waves.css" rel="stylesheet" />
    <link href="assets/plugins/animate-css/animate.css" rel="stylesheet" />
    <link href="assets/plugins/morrisjs/morris.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/themes/all-themes.css" rel="stylesheet" />
</head>

<body class="theme-blue">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.php">DASHBOARD APLIKASI ERP LABORATORY</a>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="../aset/foto/logo.png" width="100px" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Administrator</div>
                    <div class="email"><?php echo $_SESSION['Username']; ?></div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="../logout.php"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <?php
            include "menu.php";
            ?>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2018 <a href="javascript:void(0);">ERP LABORATORY <br> DEVELOPED By YAN</a>.
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>PRESENSI</h2>
            </div>
            <!-- Container -->
            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="header">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-6">
                                    <h2>REKAP PERIODE</h2>
                                </div>
                            </div>
                            <form action="" method="post">
                            <div class="row">
                              <div class="col-md-3">
                                <h5>Dari</h5>
                                  <div class="input-group">
                                    <div class="form-line">
                                      <input type="date" name="dari" id="dari" class="form-control date">
                                    </div>
                                  </div>
                              </div>
                              <div class="col-md-3">
                                <h5>Sampai</h5>
                                <div class="input-group">
                                  <div class="form-line">
                                    <input type="date" name="sampai" id="sampai" class="form-control date">
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-3">
                                <button class="btn bg-cyan btn-block waves-effect" type="submit" name="rekap">REKAP</button>
                              </div>
                            </form>
                          </div>
                        </div>

                        <div class="body table-responsive">
                          <table class="table table-striped table-hover">
                             <thead>
                             <tr>
                             <th>ID</th>
                             <th>UID</th>
                             <th>KODE ASISTEN</th>
                             <th>NAMA</th>
                             <th>DIVISI</th>
                             <th>WAKTU ABSEN</th>
                           </tr>
                         </thead>
                             <tbody>
                               <?php
                               error_reporting(E_ALL^(E_NOTICE | E_WARNING));
                               if (isset ($_POST['rekap'])){
                               $dari = $_POST['dari'];
                               $sampai = $_POST['sampai'];
                               $order = 1;
                               $queryrekap = mysqli_query($konek, "SELECT * FROM absen WHERE DATE(waktu_absen) BETWEEN '$dari 00:00:00' AND '$sampai 00:00:00'");
                               while($data = mysqli_fetch_array($queryrekap)){
                                 echo '<tr>
                                 <td>'.$order++.'</td>
                                 <td>'.$data['uid'].'</td>
                                 <td>'.$data['kode_asisten'].'</td>
                                 <td>'.$data['nama'].'</td>
                                 <td>'.$data['divisi'].'</td>
                                 <td>'.$data['waktu_absen'].'</td>
                                 ';
                               }
                             }
                                ?>
                           </tbody>
                           </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Container -->
            </div>
        </div>
    </section>
    <script src="assets/plugins/jquery/jquery.min.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.js"></script>
    <script src="assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>
    <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    <script src="assets/plugins/node-waves/waves.js"></script>
    <script src="assets/plugins/jquery-countto/jquery.countTo.js"></script>
    <script src="assets/plugins/raphael/raphael.min.js"></script>
    <script src="assets/plugins/morrisjs/morris.js"></script>
    <script src="assets/plugins/chartjs/Chart.bundle.js"></script>
    <script src="assets/plugins/flot-charts/jquery.flot.js"></script>
    <script src="assets/plugins/flot-charts/jquery.flot.resize.js"></script>
    <script src="assets/plugins/flot-charts/jquery.flot.pie.js"></script>
    <script src="assets/plugins/flot-charts/jquery.flot.categories.js"></script>
    <script src="assets/plugins/flot-charts/jquery.flot.time.js"></script>
    <script src="assets/plugins/jquery-sparkline/jquery.sparkline.js"></script>
    <script src="assets/js/admin.js"></script>
    <script src="assets/js/pages/index.js"></script>
    <script src="assets/js/demo.js"></script>
</body>

</html>
