<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <link rel="shortcut icon" href="../assets/image/ERP.png">
  <title>ERP Laboratory</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="../assets/materialize/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="../assets/materialize/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
  <nav class="light-blue lighten-1" role="navigation">
      <ul class="left hide-on-med-and-down">
        <li><img src="../assets/image/ERP white.png" width="130px"></li>
        <li class="active"><a href="modul1.php">Modul 1</a></li>
        <li><a href="modul2.php">Modul 2</a></li>
        <li><a href="modul3.php">Modul 3</a></li>
        <li><a href="modul4.php">Modul 4</a></li>
        <li><a href="modul5.php">Modul 5</a></li>
        <li><a href="modul6.php">Modul 6</a></li>
      </ul>
      <ul class="right hide-on-med-and-down">
        <li><a href="index.php">Home</a></li>
      </ul>
  </nav>
  <div align="middle">
    <div class="container">
        <div style="padding-top:20px;" class="row">
          <h1 class="heading">
              MODUL 1 INTRODUCTION SAP
          </h1>
            <div class="col s12 l4">
                <div class="card">
                    <div class="card-image blue">
                      <img src="../assets/image/ERP white.png">
                    </div>
                    <div class="card-content">
                      <p><b><h5>MODUL PRAKTIKUM</h5></p>
                      </div>
                      <div class="card-action">
                        <a class="blue-text"href="modul/M1.php">LIHAT MODUL</a>
                      </div>
                </div>
              </div>
              <div class="col s12 l4">
                  <div class="card">
                      <div class="card-image green">
                        <img src="../assets/image/ERP white.png">
                      </div>
                      <div class="card-content">
                        <p><b><h5>TEST AWAL</h5></p>
                        </div>
                        <div class="card-action">
                          <a class="blue-text" href="test/T_Awal1.php">MULAI TEST</a>
                        </div>
                  </div>
                </div>
                <div class="col s12 l4">
                    <div class="card">
                        <div class="card-image orange">
                          <img src="../assets/image/ERP white.png">
                        </div>
                        <div class="card-content">
                          <p><b><h5>TEST AKHIR</h5></p>
                          </div>
                          <div class="card-action">
                            <a class="blue-text" href="test/T_Akhir1.php" target="blank">MULAI TEST</a>
                          </div>
                    </div>
                  </div>
                <!-- End -->
            </div>
          </div>
        </div>
  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="../assets/materialize/js/materialize.js"></script>
  <script src="js/init.js"></script>

  </body>
</html>
